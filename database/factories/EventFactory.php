<?php

namespace Database\Factories;

use App\Models\Organizer;
use App\Models\Venue;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Event>
 */
class EventFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $start = fake()->unixTime();

        return [
            'venue_id' => Venue::factory(),
            'organizer_id' => Organizer::factory(),
            'name' => fake()->sentence(),
            'description' => fake()->text(),
            'begins_at' => new Carbon($start),
            'ends_at' => fake()->optional()->passthrough((new Carbon($start))->add('+5 hours')),
        ];
    }
}
