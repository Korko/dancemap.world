<?php

namespace App\Http\Controllers;

use App\Models\Event;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Laravel\Jetstream\Jetstream;

class EventsListingController extends Controller
{
    /**
     * Show the list of events screen.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Inertia\Response
     */
    public function show(Request $request)
    {
        return Jetstream::inertia()->render($request, 'Admin/Events/List', [
            'events' => Event::with('venue')->paginate(30),
        ]);
    }

    /**
     * Show the map screen centered around an event.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Inertia\Response
     */
    public function map(Request $request, Event $event)
    {
        return Jetstream::inertia()->render($request, 'Admin/Events/Map', [
            'event' => $event->load('venue'),
        ]);
    }
}
