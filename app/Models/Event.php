<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;

    /**
     * The venue of the event.
     */
    public function venue()
    {
        return $this->belongsTo(Venue::class);
    }

    /**
     * The tags of the event.
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}
