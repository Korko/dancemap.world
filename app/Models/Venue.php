<?php

namespace App\Models;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use MatanYadaev\EloquentSpatial\Objects\Point;
use MatanYadaev\EloquentSpatial\SpatialBuilder;

class Venue extends Model
{
    use HasFactory;

    protected $casts = [
        'location' => Point::class,
    ];

    public function newEloquentBuilder($query): SpatialBuilder
    {
        return new SpatialBuilder($query);
    }

    /**
     * Scope a query to only include users of a given type.
     */
    public function scopeInRange(Builder $query, Point $origin, $maxDistance): Builder
    {
        return $query->havingRaw('ST_DISTANCE_SPHERE(location, POINT(?, ?)) <= ?', [$origin->longitude, $origin->latitude, $maxDistance]);
    }
}
